package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"sync"
	"time"

	"gopkg.in/telegram-bot-api.v4"
)

const (
	stateHello       = "helloState"
	stateSchemaOffer = "schemaState"
	stateSchemaDescr = "schemaDescrState"
	stateQuestion1   = "question1State"
	stateRepeat1     = "repeat1State"
	stateQuestion2   = "question2State"
	stateRepeat2     = "repeat2State"
	stateQuestion3   = "question3State"
	stateRepeat3     = "repeat3State"
	stateQuestion4   = "question4State"
	stateRepeat4     = "repeat4State"
	stateQuestion5   = "question5State"
	stateRepeat5     = "repeat5State"
	stateFinal       = "finalState"
)

type (
	StateManager struct {
		userState map[int64]string
		sync.Mutex
		repeat  bool
		isPhoto bool
	}

	StateLog struct {
		action    string
		timestamp time.Time
	}
)

var userStateLog map[int64][]StateLog

func (stateMng *StateManager) saveState() {
	userData, err := json.Marshal(stateMng.userState)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	err = ioutil.WriteFile("files/cache/stateCache.json", userData, 0)
	if err != nil {
		log.Println("Unable to open stateCache.json: ", err)
	}

	jsonStr := string(userData)
	fmt.Println("The JSON data is:")
	fmt.Println(jsonStr)
}

func (stateMng *StateManager) initStateMng() {
	stateMng.userState = make(map[int64]string)
	stateMng.repeat = false
	stateMng.isPhoto = false
}

//SetState sets state
func (stateMng *StateManager) SetState(uid int64, state string) {
	stateMng.Lock()
	stateMng.userState[uid] = state
	stateMng.Unlock()
}

func (stateMng *StateManager) repeatMsg(uid int64) string {
	msg := ""

	switch stateMng.userState[uid] {

	case stateQuestion1:
		msg = bot.messages.Wrong1

	case stateQuestion2:
		msg = bot.messages.Wrong2

	case stateQuestion3:
		msg = bot.messages.Wrong3

	case stateQuestion4:
		msg = bot.messages.Wrong4

	case stateQuestion5:
		msg = bot.messages.Wrong5

	}
	return msg
}

func (stateMng *StateManager) photoMsg(uid int64) tgbotapi.PhotoConfig {

	msg := tgbotapi.NewPhotoUpload(uid, "")

	switch stateMng.userState[uid] {
	case stateQuestion1:
		msg = tgbotapi.NewPhotoUpload(uid, "files/img/1.jpg")
		msg.Caption = bot.messages.Question1
		msg.ReplyMarkup = getKeyboard(uid)
	case stateQuestion2:
		msg = tgbotapi.NewPhotoUpload(uid, "files/img/2.jpg")
		msg.Caption = bot.messages.Question2
		msg.ReplyMarkup = getKeyboard(uid)
	case stateQuestion3:
		msg = tgbotapi.NewPhotoUpload(uid, "files/img/3.jpg")
		msg.Caption = bot.messages.Question3
		msg.ReplyMarkup = getKeyboard(uid)
	}

	return msg
}

func (stateMng *StateManager) textMsg(uid int64) string {
	msg := ""
	switch stateMng.userState[uid] {

	case stateHello:
		msg = bot.messages.Hello

	case stateSchemaOffer:
		msg = bot.messages.GetSchema

	case stateSchemaDescr:
		msg = bot.messages.SchemaDescr

	case stateQuestion4:
		msg = bot.messages.Question4

	case stateQuestion5:
		msg = bot.messages.Question5

	case stateFinal:
		msg = bot.messages.Final

	}

	return msg
}
