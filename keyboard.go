package main

import (
	"gopkg.in/telegram-bot-api.v4"
)

func getKeyboard(uid int64) tgbotapi.InlineKeyboardMarkup {
	var keyboard tgbotapi.InlineKeyboardMarkup
	switch bot.stateMng.userState[uid] {
	case stateHello:
		{
			keyboard = tgbotapi.NewInlineKeyboardMarkup(
				tgbotapi.NewInlineKeyboardRow(
					tgbotapi.NewInlineKeyboardButtonData(wantMoneybtn.label, wantMoneybtn.callback),
				),
				tgbotapi.NewInlineKeyboardRow(
					tgbotapi.NewInlineKeyboardButtonURL(proofBtn.label, bot.ProofLink),
				),
			)
		}

	case stateSchemaOffer:
		{
			keyboard = tgbotapi.NewInlineKeyboardMarkup(
				tgbotapi.NewInlineKeyboardRow(
					tgbotapi.NewInlineKeyboardButtonData(getSchemaButton.label, getSchemaButton.callback),
				),
			)
		}

	case stateSchemaDescr:
		{
			keyboard = tgbotapi.NewInlineKeyboardMarkup(
				tgbotapi.NewInlineKeyboardRow(
					tgbotapi.NewInlineKeyboardButtonData(trainBtn.label, trainBtn.callback),
				),
			)
		}

	case stateQuestion1:
		{
			keyboard = answerKeyboardTpl(q1ABtn, q1BBtn, q1CBtn)
		}

	case stateQuestion2:
		{
			keyboard = answerKeyboardTpl(q2ABtn, q2BBtn, q2CBtn)
		}
	case stateQuestion3:
		{
			keyboard = answerKeyboardTpl(q3ABtn, q3BBtn, q3CBtn)
		}
	case stateQuestion4:
		{
			keyboard = answerKeyboardTpl(q4ABtn, q4BBtn, q4CBtn)
		}
	case stateQuestion5:
		{
			keyboard = answerKeyboardTpl(q5ABtn, q5BBtn, q5CBtn)
		}

	case stateFinal:
		{
			keyboard = tgbotapi.NewInlineKeyboardMarkup(
				tgbotapi.NewInlineKeyboardRow(
					tgbotapi.NewInlineKeyboardButtonURL(landingBtn.label, bot.Landing),
				),
				tgbotapi.NewInlineKeyboardRow(
					tgbotapi.NewInlineKeyboardButtonURL(webmoneyBtn.label, bot.Webmoney),
				),
			)
		}

	}

	if uid == bot.Root {
		keyboard = appendRootPanel(keyboard)
	}

	return keyboard
}

func repeatKeyboard(uid int64) tgbotapi.InlineKeyboardMarkup {
	keyboard := tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData(repeatBtn.label, repeatBtn.callback),
		),
	)

	return keyboard
}

func answerKeyboardTpl(a, b, c button) tgbotapi.InlineKeyboardMarkup {
	keyboard := tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData(a.label, a.callback),
			tgbotapi.NewInlineKeyboardButtonData(b.label, b.callback),
			tgbotapi.NewInlineKeyboardButtonData(c.label, c.callback),
		),
	)

	return keyboard
}

func appendRootPanel(keyboard tgbotapi.InlineKeyboardMarkup) tgbotapi.InlineKeyboardMarkup {
	tmp := make([][]tgbotapi.InlineKeyboardButton, len(keyboard.InlineKeyboard))
	tmp = keyboard.InlineKeyboard

	tmp = append(tmp, tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData(getLogBtn.label, getLogBtn.callback),
	))

	keyboard.InlineKeyboard = tmp

	return keyboard
}
