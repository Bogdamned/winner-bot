package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

type Messages struct {
	Hello       string `json:"msgHello"`
	GetSchema   string `json:"msgGetSchema"`
	SchemaDescr string `json:"msgSchemaDescr"`
	Question1   string `json:"msgQuestion1"`
	Wrong1      string `json:"msgWrong1"`
	Question2   string `json:"msgQuestion2"`
	Wrong2      string `json:"msgWrong2"`
	Question3   string `json:"msgQuestion3"`
	Wrong3      string `json:"msgWrong3"`
	Question4   string `json:"msgQuestion4"`
	Wrong4      string `json:"msgWrong4"`
	Question5   string `json:"msgQuestion5"`
	Wrong5      string `json:"msgWrong5"`
	Final       string `json:"msgFinal"`
}

func (msg *Messages) initMsg() {
	jsonFile, err := os.Open("files/messages.json")
	if err != nil {
		log.Println("Unable to open messages.json: ", err)
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, msg)
}
