package main

import (
	"os"
	"os/signal"
	"syscall"
)

func init() {
	bot.initBot()
}

func main() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt, os.Kill, syscall.SIGTERM)
	go func() {
		//Запускается при закрытии программы, для сохранения данных
		<-sigs
		bot.stateMng.saveState()
		os.Exit(1)

	}()

	bot.Polling()
}
