package main

type button struct {
	label    string
	callback string
}

var (
	wantMoneybtn    = button{label: "Хочу денег!", callback: "wantMoneyBtn"}
	getSchemaButton = button{label: "Получить схему", callback: "getSchemaBtn"}
	trainBtn        = button{label: "Тренировка", callback: "trainBtn"}
	repeatBtn       = button{label: "Повторить", callback: "repeatBtn"}

	//Question1
	q1ABtn = button{label: "530", callback: "q1ABtn"}
	q1BBtn = button{label: "557", callback: "q1BBtn"}
	q1CBtn = button{label: "501", callback: "q1CBtn"}

	//Question2
	q2ABtn = button{label: "Rock Climber", callback: "q2ABtn"}
	q2BBtn = button{label: "Garage", callback: "q2BBtn"}
	q2CBtn = button{label: "The Maney Game", callback: "q2CBtn"}

	//Question3
	q3ABtn = button{label: "1/30/1", callback: "q3ABtn"}
	q3BBtn = button{label: "3/10/1", callback: "q3BBtn"}
	q3CBtn = button{label: "1/10/3", callback: "q3CBtn"}

	//Question4
	q4ABtn = button{label: "5/105/1", callback: "q4ABtn"}
	q4BBtn = button{label: "5/100/5", callback: "q4BBtn"}
	q4CBtn = button{label: "5/100/1", callback: "q4CBtn"}

	//Question5
	q5ABtn = button{label: "9/180/9", callback: "q5ABtn"}
	q5BBtn = button{label: "9/190/8", callback: "q5BBtn"}
	q5CBtn = button{label: "9/180/1", callback: "q5CBtn"}

	landingBtn  = button{label: "В игру", callback: "landingBtn"}
	webmoneyBtn = button{label: "Вебмани", callback: "webmoneyBtn"}

	getLogBtn = button{label: "Выгрузить логи действий", callback: "getActionLogBtn"}

	proofBtn = button{label: "Обо мне", callback: "http://korrespondent.net/lifestyle/gadgets/4011141-khaker-vzlomal-onlain-kazyno-y-24-raza-sorval-dzhekpot"}
)
