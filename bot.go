//Token: 668411529:AAHn1MmLvQGk9YntJu_LWIa85YoZpAElWoA
package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/tealeg/xlsx"
	"gopkg.in/telegram-bot-api.v4"
)

type (
	Bot struct {
		botAPI    *tgbotapi.BotAPI
		stateMng  StateManager
		messages  Messages
		Token     string `json:"token" bson:"token"`
		Landing   string `json:"landing" bson:"landing"`
		Webmoney  string `json:"webmoney" bson:"webmoney"`
		Root      int64  `json:"root"`
		ProofLink string `json:"proof"`
	}
	ActionLogRec struct {
		UserID int64     `json:"uid"`
		Action string    `json:"action"`
		Time   time.Time `json:"time"`
	}
)

var bot = Bot{}

func (bot *Bot) initBot() {
	jsonFile, err := os.Open("files/settings.json")
	if err != nil {
		log.Println("Unable to open settings.json: ", err)
	}
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &bot)

	bot.botAPI, err = tgbotapi.NewBotAPI(bot.Token)
	if err != nil {
		log.Fatal("Failed to connect to bot :", err.Error())
	}

	bot.botAPI.Debug = false

	bot.stateMng.initStateMng()
	bot.messages.initMsg()

	log.Printf("Authorized on account %s", bot.botAPI.Self.UserName)

	// msg := tgbotapi.NewMessage(766917128, "Привет, Зеленый Фонарь, это Бэтмен")
	// _, erro := bot.botAPI.Send(msg)
	// if erro != nil {
	// 	log.Println("Ненене ", erro.Error())
	// }
}

//Polling starts polling
func (bot *Bot) Polling() {
	config := tgbotapi.NewUpdate(0)
	config.Timeout = 60
	updates, err := bot.botAPI.GetUpdatesChan(config)
	if err != nil {
		log.Println("[ERROR] Unable to get updates: ", err)
	}

	for {
		select {
		case update := <-updates:
			if update.Message != nil {
				bot.processMsg(&update)
			} else {
				bot.processCallback(&update)
			}
		}
	}
}

func (bot *Bot) processMsg(update *tgbotapi.Update) {
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")

	switch update.Message.Text {
	case "/start":
		if _, exists := bot.stateMng.userState[update.Message.Chat.ID]; !exists {
			bot.stateMng.SetState(update.Message.Chat.ID, stateHello)
			bot.logActionXLS(update.Message, "/start")
			msg.ReplyMarkup = getKeyboard(update.Message.Chat.ID)
			msg.Text = bot.messages.Hello
			bot.botAPI.Send(msg)
		}
	}
}

func (bot *Bot) processCallback(update *tgbotapi.Update) {
	msg := tgbotapi.NewMessage(update.CallbackQuery.Message.Chat.ID, "")
	btnLabel := ""

	switch update.CallbackQuery.Data {
	case wantMoneybtn.callback:
		btnLabel = wantMoneybtn.label
		bot.stateMng.SetState(update.CallbackQuery.Message.Chat.ID, stateSchemaOffer)

	case getSchemaButton.callback:
		btnLabel = getSchemaButton.label
		bot.stateMng.SetState(update.CallbackQuery.Message.Chat.ID, stateSchemaDescr)

	case trainBtn.callback:
		btnLabel = trainBtn.label

		bot.stateMng.isPhoto = true
		bot.stateMng.SetState(update.CallbackQuery.Message.Chat.ID, stateQuestion1)

	//Question 1: answer B
	case q1ABtn.callback:
		btnLabel = q1ABtn.label
		bot.stateMng.repeat = true
	case q1CBtn.callback:
		btnLabel = q1CBtn.label
		bot.stateMng.repeat = true
	case q1BBtn.callback:
		btnLabel = q1BBtn.label
		bot.stateMng.SetState(update.CallbackQuery.Message.Chat.ID, stateQuestion2)

	//Question 2: answer C
	case q2ABtn.callback:
		btnLabel = q2ABtn.label
		bot.stateMng.repeat = true
	case q2BBtn.callback:
		btnLabel = q2BBtn.label
		bot.stateMng.repeat = true
	case q2CBtn.callback:
		btnLabel = q2CBtn.label
		bot.stateMng.SetState(update.CallbackQuery.Message.Chat.ID, stateQuestion3)

	//Question 3: answer A
	case q3BBtn.callback:
		btnLabel = q3BBtn.label
		bot.stateMng.repeat = true
	case q3CBtn.callback:
		btnLabel = q3CBtn.label
		bot.stateMng.repeat = true
	case q3ABtn.callback:
		btnLabel = q3ABtn.label
		bot.stateMng.isPhoto = false
		bot.stateMng.SetState(update.CallbackQuery.Message.Chat.ID, stateQuestion4)

	//Question 4: answer C
	case q4ABtn.callback:
		btnLabel = q4ABtn.label
		bot.stateMng.repeat = true
	case q4BBtn.callback:
		btnLabel = q4BBtn.label
		bot.stateMng.repeat = true
	case q4CBtn.callback:
		btnLabel = q4CBtn.label
		bot.stateMng.SetState(update.CallbackQuery.Message.Chat.ID, stateQuestion5)

	//Question 5: answer C
	case q5ABtn.callback:
		btnLabel = q5ABtn.label
		bot.stateMng.repeat = true
	case q5BBtn.callback:
		btnLabel = q5BBtn.label
		bot.stateMng.repeat = true
	case q5CBtn.callback:
		btnLabel = q5CBtn.label
		bot.stateMng.SetState(update.CallbackQuery.Message.Chat.ID, stateFinal)

	case repeatBtn.callback:
		btnLabel = repeatBtn.label
		bot.stateMng.repeat = false

	case getLogBtn.callback:
		//выгружаем в ексель
		log := bot.logToExcel()
		bot.botAPI.Send(log)

		return

	default:
		msg.Text = "/start"
	}

	bot.reply(update, msg)
	bot.logActionCSV(update.CallbackQuery.Message.Chat.ID, btnLabel)
	bot.logActionXLS(update.CallbackQuery.Message, btnLabel)
}

func (bot *Bot) logUser(uid int64) {

}

func (bot *Bot) reply(update *tgbotapi.Update, msg tgbotapi.MessageConfig) {
	if bot.stateMng.repeat {
		msg.ReplyMarkup = repeatKeyboard(update.CallbackQuery.Message.Chat.ID)
		msg.Text = bot.stateMng.repeatMsg(update.CallbackQuery.Message.Chat.ID)

		bot.stateMng.repeat = false
		bot.botAPI.Send(msg)
	} else {
		if bot.stateMng.isPhoto {
			msg := bot.stateMng.photoMsg(update.CallbackQuery.Message.Chat.ID)
			bot.botAPI.Send(msg)
		} else {
			msg.ReplyMarkup = getKeyboard(update.CallbackQuery.Message.Chat.ID)
			msg.Text = bot.stateMng.textMsg(update.CallbackQuery.Message.Chat.ID)

			bot.botAPI.Send(msg)
		}
	}

}

func (bot *Bot) logActionCSV(uid int64, btnLabel string) {
	// Open a csv file
	f, err := os.OpenFile("files/logs/userActionLog.csv", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)

	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()

	w := csv.NewWriter(f)

	var record []string
	record = append(record, strconv.FormatInt(uid, 10))
	record = append(record, "Pressed button: "+btnLabel)
	record = append(record, time.Now().String())
	w.Write(record)

	for _, v := range record {
		log.Print(v)
	}

	log.Println()
	w.Flush()
}

func (bot *Bot) logActionXLS(msg *tgbotapi.Message, btnLabel string) {
	var file *xlsx.File
	var sheet *xlsx.Sheet
	var row *xlsx.Row
	var cell *xlsx.Cell
	var err error

	file, err = xlsx.OpenFile("files/logs/userLog.xlsx")
	//file = xlsx.NewFile()
	sheet = file.Sheet["User activiy log"]
	if err != nil {
		fmt.Printf(err.Error())
	}
	row = sheet.AddRow()

	//UserID
	cell = row.AddCell()
	cell.Value = strconv.FormatInt(msg.Chat.ID, 10)

	//Username
	cell = row.AddCell()
	cell.Value = msg.From.UserName

	//Button
	cell = row.AddCell()
	cell.Value = btnLabel

	//State
	cell = row.AddCell()
	cell.Value = bot.stateMng.userState[msg.Chat.ID]

	//Timestamp
	cell = row.AddCell()
	cell.Value = time.Now().String()

	err = file.Save("files/logs/userLog.xlsx")
	if err != nil {
		fmt.Printf(err.Error())
	}

	// Open a csv file
	// f, err := os.OpenFile("files/logs/userActionLog.csv", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)

	// xls.NewStyle()

	// if err != nil {
	// 	fmt.Println(err)
	// }
	// defer f.Close()

	// w := csv.NewWriter(f)

	// var record []string
	// record = append(record, strconv.FormatInt(uid, 10))
	// record = append(record, "Pressed button: "+btnLabel)
	// record = append(record, time.Now().String())
	// w.Write(record)

	// for _, v := range record {
	// 	log.Print(v)
	// }

	// log.Println()
	// w.Flush()
}

func (bot *Bot) logToExcel() tgbotapi.DocumentConfig {
	msg := tgbotapi.NewDocumentUpload(bot.Root, "files/logs/userLog.xlsx")
	return msg
}
